#!/usr/bin/env python

from setuptools import setup
from setuptools import find_packages

import bdaq53

version = bdaq53.__version__

author = "Michael Daas, Yannick Dieter, Tomasz Hemperek, David-Leon Pohl, Mark Standke, Marco Vogt"
author_email = ""

# Requirements
install_requires = [
    "basil-daq>=3.2.0,<3.4.0",
    "charset-normalizer<3.0,>=2.0",
    "chardet<4.0",
    "bitarray>=2.0.0",
    "lxml",
    "matplotlib>=3.6.2",
    "numpy",
    "online_monitor>=0.6",
    "pillow",
    "pixel_clusterizer==3.1.7",
    "tables",
    "pyyaml",
    "pyzmq",
    "scipy",
    "numba",
    "tqdm",
    "pyserial",
    "slackclient>=2.0.1",
    "gitpython",
    "pexpect",
    "coloredlogs",
    "uncertainties",
    "requests",
    "pymongo",
    "iminuit",
    "pysftp",
    "xlrd==1.2.0",
    "XlsxWriter",
    "pyvisa_py",
    "openpyxl",
    "pybind11",
    "itkpix-efuse-codec>=0.3.3",
    "influxdb",
    "colorama",
    "seaborn"
]  # Aditional requirements for localDB module

setup(
    name="bdaq53",
    version=version,
    description="DAQ for RD53A prototype",
    url="https://gitlab.cern.ch/silab/bdaq53",
    license="",
    long_description="",
    author=author,
    maintainer=author,
    author_email=author_email,
    maintainer_email=author_email,
    install_requires=install_requires,
    python_requires=">=3.0",
    setup_requires=["online_monitor>=0.6"],
    packages=find_packages(),
    include_package_data=True,
    platforms="any",
    entry_points={
        "console_scripts": [
            "bdaq = bdaq53.bdaq53_cli:main",
            "bdaq_localdb = localdb.manage_localdb:main",
            "bdaq_firmware = bdaq53.manage_firmware:main",
            "bdaq_monitor = bdaq53.online_monitor.start_bdaq53_monitor:main",
            "bdaq_eudaq = bdaq53.scans.scan_eudaq:main",
            "bdaq_config = bdaq53.manage_configuration:main",
            "bdaq_db = bdaq53.manage_databases:main",
            "bdaq_fixtures = bdaq53.tests.create_fixtures:main",
            "start_interlock = bdaq53.module_testing.interlock_environment:start",
            "stop_interlock = bdaq53.module_testing.interlock_environment:stop",
            # Old commands left for backwards compatibility
            "bdaq53 = bdaq53.bdaq53_cli:deprecated",
            "bdaq53_monitor = bdaq53.bdaq53_cli:deprecated",
            "bdaq53_eudaq = bdaq53.bdaq53_cli:deprecated",
        ]
    },
)
